/**
 * @desc Switch tabs via buttons
 * @desc Location is 'Our Amazing Work'
 * @desc Load more images in tab 'All'
 */
let btnLoadMore1 = document.querySelector('.our-amazing-work-load-more-btn');
let loader1 = document.querySelector('.loader1');
let btnClicked = 0;

btnLoadMore1.addEventListener('click', function () {
    btnLoadMore1.style.display = 'none';
    loader1.style.display = 'block';

    setTimeout(function () {
        btnLoadMore1.style.display = 'block';
        btnLoadMore1.style.margin = 'auto';
        loader1.style.display = 'none';
    }, 1500);

    setTimeout(function () {
        btnClicked += 1;

        if (btnClicked === 1) {
            document.querySelector('.our-amazing-work-gallery-img-all-part-two').style.display = 'block';
        } else if (btnClicked === 2) {
            document.querySelector('.our-amazing-work-gallery-img-all-part-three').style.display = 'block';
            btnLoadMore1.remove();
            document.querySelector('.our-amazing-work-gallery').style.marginBottom = '-150px'
        }
    }, 1500);
});

/**
 * @desc Load more images in tab 'All'
 * @desc Location is 'Gallery og best images'
 */
let btnLoadMore2 = document.querySelector('.gallery-of-best-images-load-more-btn');
let loader2 = document.querySelector('.loader2');

btnLoadMore2.addEventListener('click', function () {
    btnLoadMore2.style.display = 'none';
    loader2.style.display = 'block';

    setTimeout(function () {
        btnLoadMore2.style.display = 'block';
        btnLoadMore2.style.margin = 'auto';
        loader2.style.display = 'none';
    }, 1500);

    // setTimeout(function () {
    //     btnClicked += 1;
    //
    //     if (btnClicked === 1) {
    //         document.querySelector('.our-amazing-work-gallery-img-all-part-two').style.display = 'block';
    //     } else if (btnClicked === 2) {
    //         document.querySelector('.our-amazing-work-gallery-img-all-part-three').style.display = 'block';
    //         btnLoadMore1.remove();
    //         document.querySelector('.our-amazing-work-gallery').style.marginBottom = '-150px'
    //     }
    // }, 1500);
});