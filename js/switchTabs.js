/**
 * @desc Switch tab's description via buttons
 * @desc Location is 'Our Service'
 */
document.querySelector('.our-service-menu-btn').classList.add('active');
document.querySelector('.our-service-content-block-txt').classList.add('active');

document.querySelectorAll('.our-service-menu-btn').forEach(el => {
    el.addEventListener('click', selectFirstTabs1);
});

function selectFirstTabs1(event) {
    let target = event.target.dataset.target;
    if (target.tagName == 'SPAN') return;

    document.querySelectorAll('.our-service-menu-btn').forEach(el => {
        el.classList.remove('active');
    });

    document.querySelectorAll('.our-service-content-block-txt').forEach(el => {
        el.classList.remove('active');
    });

    event.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}

/**
 * @desc Switch image's tabs via buttons
 * @desc Location is 'Our Amazing Work'
 */
document.querySelector('.our-amazing-work-tabs-tab').classList.add('active');
document.querySelector('.our-amazing-work-gallery-img').classList.add('active');

document.querySelectorAll('.our-amazing-work-tabs-tab').forEach(el => {
    el.addEventListener('click', selectFirstTabs2);
});

function selectFirstTabs2(event) {
    let target = event.target.dataset.target;
    if (target.tagName == 'SPAN') return;

    document.querySelectorAll('.our-amazing-work-tabs-tab').forEach(el => {
        el.classList.remove('active');
    });

    document.querySelectorAll('.our-amazing-work-gallery-img').forEach(el => {
        el.classList.remove('active');
    });

    event.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}

/**
 * @desc Switch image's tabs :hover
 * @desc Location is 'Our Amazing Work'
 */
let imgPart = document.querySelectorAll('.our-amazing-work-gallery-img-part-cls-for-js');

Array.from(imgPart).map((x) => x.addEventListener('mouseover', function () {
    this.children[0].style.display = 'none';
    this.children[1].style.display = 'block';
}));

Array.from(imgPart).map((x) => x.addEventListener('mouseleave', function () {
    this.children[1].style.display = 'none';
    this.children[0].style.display = 'block';
}));